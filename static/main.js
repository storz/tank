enchant();
var game;
var ws;
var isTouch = false;
var touchX;
var touchY;
var isFixed = false;
var isThereBullet = false;
var wsFlag = false;
var uuid;
var player;
var enemy = [];
var explosion = [];
var myBullet =[];
var enemyBullet = [];
var item = [];
var bid = 0;
var iid = 0;
var b = 0;
var count = 1;
var score;
var scoreBoard = [];
var myName;

myName = prompt('名前を入力して下さい', '');
if (myName == null) myName = '';

url = 'ws://' + location.host + '/websocket';
if ("WebSocket" in window) {
  ws = new WebSocket(url);
} else if ("MozWebSocket" in window) {
  ws = new MozWebSocket(url);
}

ws.onopen = function (evt) {
  wsFlag = true;
  console.log('connect');
  game = new Game(800, 800);
  game.preload('static/images/map0.png', 'static/images/chara3.png', 'static/images/effect0.png', 'static/images/icon0.png');
  game.fps = 30;

  var bullet = [];
  var msg = {
    'type' : 'join',
    'name' : myName,
  };
  ws.send(JSON.stringify(msg));
  wsConnected();

  game.onload = function(){
    var map = new Map(16, 16);
    var collisionmap;
    map.image = game.assets['static/images/map0.png'];
    var basemap = new Array(game.height/16);
    for (var i = 0; i < game.height; i++) {
      basemap[i] = new Array(game.width)
      for (var j = 0; j < game.width; j++) {
        basemap[i][j] = 0;
      }
    }
    var collisionmap = new Array(basemap.length);
    for (var i = 0; i < basemap.length; i++) {
      collisionmap[i] = new Array(basemap[0].length)
      for (var j = 0; j < basemap[0].length; j++) {
        if (basemap[i][j] !== 0) {
          collisionmap[i][j] = 1;
        } else {
          collisionmap[i][j] = 0;
        }
      }
    }
    map.loadData(basemap);
    map.collisionData = collisionmap;
    game.rootScene.addChild(map);


    player = new Player(Math.random()*game.width, Math.random()*game.height);

    score = new Label('score : ' + player.score);
    score.font = "16px Helvetica";
    score.x = 10;
    score.y = 10;
    game.rootScene.addChild(score);

    ranking = new Label();
    ranking.font = "16px Helvetica";
    ranking.x = 650;
    ranking.y = 10;
    game.rootScene.addChild(ranking);

    // クリックイベントを登録

    game.rootScene.addEventListener("touchstart", function(e) {
      isTouch = true;
      touchX = e.localX;
      touchY = e.localY;
      player.sendTrack(touchX, touchY, player.speed);
    }); // タッチ開始
    game.rootScene.addEventListener("touchend", function(e) {
      isTouch = false;
      touchX = e.localX;
      touchY = e.localY;
      player.sendTrack(touchX, touchY, player.speed);
    });  // タッチ終了
    game.rootScene.addEventListener("touchmove", function(e) {
      // タッチ位置をセット
      touchX = e.localX;
      touchY = e.localY;
      player.sendTrack(touchX, touchY, player.speed);
    });

    // スペースキーの挙動
    game.keybind(32, "space");
    game.addEventListener("spacebuttondown", function(e) {
      isFixed = true;
      player.sendTrack(player.x, player.y, 0);
    }); // 動きを固定
    game.addEventListener("spacebuttonup", function(e) {
      isFixed = false;
      player.sendTrack(touchX, touchY, player.speed);
    }); // 動きの固定解除

    game.rootScene.addEventListener('enterframe', function(e) {
      if (count % 120 === 0) {
        var msg = {
          'type' : 'fetch',
          'id' : uuid,
          'score' : player.score,
          'x' : player.x,
          'y' : player.y,
          'speed' : player.speed,
          'r' : player.rotation
        };
        ws.send(JSON.stringify(msg));
      }
      if (count % 600 === 0) {
        if (!item[uuid]) item[uuid] = [];
        item[uuid][iid] = new RapidFire(Math.random()*game.width, Math.random()*game.height, uuid, iid, 'rf');
        count = 1;
      }
      count++;
    });
  };
  game.start(); // start your game!
};

var Tank = enchant.Class.create(enchant.Sprite, {
  initialize: function(x, y) {
    enchant.Sprite.call(this, 32, 32);
    this.x = x;
    this.y = y;
    this.vx = x;
    this.vy = y;
    this.score;
    this.pilotName;
    //this.isInvincible = true;
    this.invincible();
    this.addEventListener('enterframe', function(e) {
      if (this.isInvincible) {
        if (this.opacity > 0.7) this.opacity = 0.4;
        else if (this.opacity < 0.5) this.opacity = 0.8;
      }
    });
    game.rootScene.addChild(this);     // add to canvas
  },
  invincible: function () {
    setTimeout(function() {
      this.isInvincible = false;
      this.opacity = 1.0;
    }, 5000);
  },
  move: function() {
    this.x += this.vx * this.speed;
    this.y += this.vy * this.speed;
  },
  vanish: function() {
    game.rootScene.removeChild(this);
    delete this;
  }
});

var Player = enchant.Class.create(Tank, {
  initialize: function(x, y) {
    Tank.call(this, 32, 32);
    this.image = game.assets['static/images/chara3.png'];
    this.frame = 19;
    this.x = x;
    this.y = y;
    this.speed = 2;
    this.bulletInterval = 1000;
    touchX = x + 16;
    touchY = y + 16;
    this.score = 0;
    this.pilotName = '';
    this.isAbleToShoot = true;
    this.isAbleToMove = true;
    var data = {
      'type': 'new',
      'obj': 'tank',
      'id': uuid,
      'x': x,
      'y': y,
      'speed': this.speed,
      'r': this.rotation
    };
    ws.send(JSON.stringify(data));
    this.addEventListener('enterframe', function(e) {
      this.vx = touchX - this.x - 16;
      this.vy = touchY - this.y - 16;
      var angle = Math.atan2(this.vy, this.vx);
      this.rotation = angle / Math.PI * 180 + 90;
      var lengthSquared = (this.vx*this.vx + this.vy*this.vy);
      var len = Math.sqrt(lengthSquared);
      if (isTouch && this.isAbleToShoot) {
        myBullet[bid] = new MyBullet(this.x, this.y, this.vx/len, this.vy/len, bid);
        this.isAbleToShoot = false;
        charge();
      }
      if (this.isAbleToMove === true) {
        if (lengthSquared > this.speed*this.speed && isFixed === false) {
          // 正規化
          this.vx /= len;
          this.vy /= len;
          this.move();
        }
      }
    });
    this.addEventListener("removed", function(e) {
      console.log('removed');
    });
  },
  destroy: function(x, y, k) {
    console.log('yarareta');
    var data = {
      'type': 'destroy',
      'id': uuid,
      'killer': k,
      'x': player.x,
      'y': player.y
    };
    ws.send(JSON.stringify(data));
    if (!item[uuid]) item[uuid] = [];
    item[uuid][iid] = new SpeedUp(player.x, player.y, uuid, iid, 'su');
    game.rootScene.removeChild(this);
    this.initialize(Math.random()*300, Math.random()*300);
    //delete this;
    explosion[b] = new Explosion(x, y);
    b++;
  },
  sendTrack: function(touchX, touchY, speed) {
    if (isFixed) speed = 0;
    var data = {
      'type': 'track',
      'obj': 'tank',
      'id': uuid,
      'x': this.x,
      'y': this.y,
      'vx': touchX,
      'vy': touchY,
      'speed': speed,
      'r': this.rotation
    };
    ws.send(JSON.stringify(data));
  }
});

var Enemy = enchant.Class.create(Tank, {
  initialize: function(x, y, speed, r){
    Tank.call(this, x, y);
    this.image = game.assets['static/images/chara3.png'];
    this.frame = 23;
    this.vx = x;
    this.vy = y;
    this.tX;
    this.tY;
    this.rotation = r;
    this.speed = speed;
    this.isMoving = false;
    this.addEventListener('enterframe', function(e) {
      if (this.isMoving) {
        this.vx = this.tX - this.x - 16;
        this.vy = this.tY - this.y - 16;
        var lengthSquared = (this.vx*this.vx + this.vy*this.vy);
        var len = Math.sqrt(lengthSquared);
        if (lengthSquared > this.speed*this.speed && isFixed === false) {
          this.vx /= len;
          this.vy /= len;
          this.move();
        }
      }
    });
  },
  receiveTrack: function(x, y, tX, tY, speed, r) {
    this.x = x;
    this.y = y;
    this.tX = tX;
    this.tY = tY;
    this.rotation = r;
    this.speed = speed;
    if (speed !== 0) this.isMoving = true;
    else this.isMoving = false;
  },
  destroy: function(x, y) {
    game.rootScene.removeChild(this);
    delete this;
    explosion[b] = new Explosion(x, y);
    b++;
  }
});

var Bullet = enchant.Class.create(enchant.Sprite, {
  initialize: function(x, y, mx, my){
    enchant.Sprite.call(this, 16, 16);
    this.image = game.assets['static/images/icon0.png'];
    //this.angle = rot;
    this.x = x + 9;
    this.y = y + 9;
    this.speed = 7;
    //this.tl.moveBy(mx, my, 10);
    this.frame = 45;
    this.isActive = true;
    this.tl.moveBy(mx*200*this.speed, my*200*this.speed, 200);
    this.addEventListener('enterframe', function(e) {
      if ((this.x < 0) || (this.y < 0) || (this.x > game.width) || (this.y > game.height)) {
        this.vanish();
      }
    });
    game.rootScene.addChild(this);
  },
  move: function(x, y) {
    this.x = x,
    this.y = y
  },
  vanish: function() {
    this.isActive = false;
    game.rootScene.removeChild(this);
    delete this;
  }
});

var MyBullet = enchant.Class.create(Bullet, {
  initialize: function(x, y, mx, my, bulletid) {
    Bullet.call(this, x, y, mx, my);
    this.bid = bulletid;
    var data = {
      'type' : 'new',
      'obj' : 'bullet',
      'id' : uuid,
      'bid' : bulletid,
      'x' : x,
      'y' : y,
      'mx' : mx,
      'my' : my,
      'speed' : this.speed,
      'f' : 100
    };
    this.addEventListener('enterframe', function(e) {
      if ((this.x < 0) || (this.y < 0) || (this.x > game.width) || (this.y > game.height)) {
        this.vanish();
        var data = {
          'type': 'vanish',
          'obj': 'bullet',
          'id': uuid,
          'bid': this.bid
        };
        ws.send(JSON.stringify(data));
      }
    });
    ws.send(JSON.stringify(data));
    bid++;
  }
});

var EnemyBullet = enchant.Class.create(Bullet, {
  initialize: function(x, y, mx, my, id, bulletid) {
    Bullet.call(this, x, y, mx, my);
    this.frame = 46;
    this.id = id;
    this.bid = bulletid;
    this.addEventListener('enterframe', function(e) {
      if(player.within(this, 16)) {
        if (this.isActive && !player.isInvincible) {
          this.vanish();
          var data = {
            'type': 'vanish',
            'obj': 'bullet',
            'id': this.id,
            'bid': this.bid
          };
          ws.send(JSON.stringify(data));
          player.destroy(player.x, player.y, this.id);
        }
      }
    })
  }
});

var Explosion = enchant.Class.create(enchant.Sprite, {
  initialize: function(x, y){
    enchant.Sprite.call(this, 16, 16);
    this.image = game.assets['static/images/effect0.png'];
    this.frame = 0;
    this.moveTo(x+8, y+8);
    this.addEventListener('enterframe', function(e) {
      explosion.forEach(function(blast) {
        blast.frame++;
        if(blast.frame > 4) {
          game.rootScene.removeChild(blast);
          delete blast;
        }
      });
    });
    game.rootScene.addChild(this);   // add to canvas
  }
});

var Item = enchant.Class.create(enchant.Sprite, {
  initialize: function(x, y, id, itemid, fx) {
    enchant.Sprite.call(this, 16, 16);
    this.image = game.assets['static/images/icon0.png'];
    this.moveTo(x, y);
    this.id = id;
    this.iid = itemid;
    if (id === uuid) {
      var data = {
        'type' : 'new',
        'obj' : 'item',
        'id' : this.id,
        'iid' : itemid,
        'x' : x,
        'y' : y,
        'effect' : fx
      };
      ws.send(JSON.stringify(data));
    }
    this.addEventListener('enterframe', function(e) {
      if(player.within(this, 16)) {
        this.effect();
        var data = {
          'type': 'vanish',
          'obj': 'item',
          'id': this.id,
          'iid': this.iid
        };
        ws.send(JSON.stringify(data));
        this.vanish();
      }
    });
    game.rootScene.addChild(this);
    iid++;
  },
  vanish: function() {
    game.rootScene.removeChild(this);
    delete this;
  }
});

var SpeedUp = enchant.Class.create(Item, {
  initialize: function(x, y, id, itemid, fx) {
    Item.call(this, x, y, id, itemid, fx);
    this.frame = 12;
  },
  effect: function() {
    player.speed += 0.3;
  }
});

var RapidFire = enchant.Class.create(Item, {
  initialize: function(x, y, id, itemid, fx) {
    Item.call(this, x, y, id, itemid, fx);
    this.frame = 13;
  },
  effect: function() {
    player.bulletInterval /= 1.25;
  }
});

function charge() {
  setTimeout(function() {
    player.isAbleToShoot = true;
  }, player.bulletInterval);
}

function wsConnected() {
  ws.onmessage = function (evt) {
    var json = JSON.parse(evt.data);
    if (json['type'] === 'refresh') {
      refresh(json['data']);
    } else if (json['type'] === 'init') {
      uuid = json['id'];
      init(json['data']);
    } else {
      proc(json);
    }
  };

  ws.onclose = function (evt) {
    wsFlag = false;
    var msg = {
      'type' : 'defect',
      'id' : uuid
    };
    ws.send(JSON.stringify(msg));
    console.log('close');
  };
}

function init(data) {
  for (var id in data) {
    if (id === uuid) player.pilotName = uuid.split('-')[0];//自機はスキップ
    enemyBullet[id] = [];
    item[id] = [];
    for (var n in data[id]) {
      if (n === 'tank') {
        enemy[id] = new Enemy(data[id][n][0], data[id][n][1], data[id][n][2], data[id][n][3]);
        enemy[id].score = data[id]['score'];
        enemy[id].pilotName = data[id]['name'];
        scoreBoard[data[id]['name']] = data[id]['score'];
      } else if (n === 'item') {
        for (var i in data[id][n]) {
          if (data[id][n][i][2] === 'su') item[id][i] = new SpeedUp(data[id][n][i][0], data[id][n][i][1], id, i, data[id][n][i][2]);
          else if (data[id][n][i][2] === 'rf') item[id][i] = new RapidFire(data[id][n][i][0], data[id][n][i][1], id, i, data[id][n][i][2]);
        }
      }
    }
  }
  ranking.text = '';
  for (var pn in scoreBoard) {
    ranking.text += pn + ':' + scoreBoard[pn] + '<br>';
  }
}

function refresh(data) {
  console.log('refresh');
  for (var id in data) {
    if (id === uuid) {
      for (var n in data[id]) {
        if (n === 'score') {
          player.pilotName = data[id]['name'];
          player.score = data[id]['score'];
          score.text = 'score : ' + player.score;
          scoreBoard[player.pilotName] = player.score;
        }
      }
    } else {
      for (var n in data[id]) {
        if (n === 'tank') {
          var tank = enemy[id];
          if (!tank) {
            enemy[id] = new Enemy(data[id][n][0], data[id][n][1], data[id][n][2], data[id][n][3]);
            enemy[id].score = data[id]['score'];
            enemy[id].pilotName = data[id]['name'];
          }
          scoreBoard[data[id]['name']] = data[id]['score'];
        }
      }
    }
  }
  ranking.text = '';
  for (var pn in scoreBoard) {
    ranking.text += pn + ':' + scoreBoard[pn] + '<br>';
  }
}

function proc(data) {
  if (data['type'] === 'new') {
    var id = data['id'];
    if (id !== uuid) {
      if (data['obj'] === 'tank') {
        enemy[id] = new Enemy(data['x'], data['y'], data['speed'], data['r']);
        console.log('new enemy');
        enemyBullet[id] = [];
        if (!item[id]) item[id] = [];
      } else if (data['obj'] === 'bullet') {
        enemyBullet[id][data['bid']] = new EnemyBullet(data['x'], data['y'], data['mx'], data['my'], id, data['bid']);
      } else if (data['obj'] === 'item') {
        if (data['effect'] === 'su') item[id][data['iid']] = new SpeedUp(data['x'], data['y'], id, data['iid'], 'su');
        else if (data['effect'] === 'rf') item[id][data['iid']] = new RapidFire(data['x'], data['y'], id, data['iid'], 'rf');
      }
    }
  } else if (data['type'] === 'track') {
    var id = data['id'];
    if (id != uuid) {
      if (data['obj'] === 'tank') {
        enemy[id].receiveTrack(data['x'], data['y'], data['vx'], data['vy'], data['speed'], data['r']);
      }
    }
  } else if (data['type'] === 'destroy') {
    var id = data['id'];
    if (id === uuid) {
      console.log('I am destroyed');
    } else {
      if (data['killer'] === uuid) {
        player.score += 100;
        score.text = 'score : ' + player.score;
        console.log('I killed', id);
      }
      enemy[id].destroy(data['x'], data['y']);
    }
  } else if (data['type'] === 'vanish') {
    var id = data['id'];
    if (id !== uuid) {
      if (data['obj'] === 'tank') {
        enemy[id].vanish();
      } else if (data['obj'] === 'bullet') {
        enemyBullet[id][data['bid']].vanish();
      } else if (data['obj'] === 'item') {
        item[id][data['iid']].vanish();
      }
    } else {
      if (data['obj'] === 'tank') {
      } else if (data['obj'] === 'bullet') {
        myBullet[data['bid']].vanish();
      } else if (data['obj'] === 'item') {
        item[id][data['iid']].vanish();
      }
    }
  } else if (data['type'] === 'defect') {
    console.log(data['name'], 'is defected');
    delete scoreBoard[data['name']];
    enemy[data['id']].vanish();
    for (var eb in enemyBullet[data['id']]) {
      enemyBullet[data['id']][eb].vanish();
    }
    for (var it in item[data['id']]) {
      item[data['id']][it].vanish();
    }
  }
}