#!/usr/bin/env python

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.websocket
import tornado.escape
import os.path
import json
import uuid

room = {}

from tornado.options import define, options

define("port", default=8888, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", MainHandler),
            (r"/websocket", TankServer),
        ]
        settings = dict(
            cookie_secret="43oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            static_path=os.path.join(os.path.dirname(__file__), "static"),
            xsrf_cookies=True,
            autoescape=None,
        )
        tornado.web.Application.__init__(self, handlers, **settings)


class TankServer(tornado.websocket.WebSocketHandler):
    waiters = set()
    myid = None

    @classmethod
    def send_updates(cls, message):
        for waiter in cls.waiters:
            try:
                waiter.write_message(message)
                print 'update'
            except:
                print 'update error'

    def open(self):
        print "opened"
        self.myid = str(uuid.uuid4())
        self.connection_closed = False
        TankServer.waiters.add(self)

    def on_message(self, message):
        if not self.connection_closed:
            try:
                print message
                m = json.loads(message)
                if m['type'] == 'join':
                    self.write_message(json.dumps({'type': 'init', 'id': self.myid, 'data': room}))
                    if m['name'] == '':
                        m['name'] = self.myid.split('-')[0]
                    room.update({self.myid: {'tank': [], 'bullet': {}, 'item': {}, 'score': 0, 'name': m['name']}})
                    print 'joined'
                if m['type'] == 'fetch':
                    room[self.myid]['tank'] = [m['x'], m['y'], m['speed'], m['r']]
                    room[self.myid]['score'] = m['score']
                    self.write_message(json.dumps({'type': 'refresh', 'data': room}))
                if m['type'] == 'new':
                    print 'new'
                    if m['obj'] == 'tank':
                        if 'id' not in m:
                            m['id'] = self.myid
                        message = json.dumps(m)
                        room[self.myid]['tank'] = [m['x'], m['y'], m['speed'], m['r']]
                        room[self.myid]['score'] = 0;
                    if m['obj'] == 'bullet':
                        if not m['mx']:
                            room[self.myid]['bullet'][m['bid']] = [m['x'], m['y'], m['mx'], m['my'], m['speed'], m['f']]
                    if m['obj'] == 'item':
                        print 'new item'
                        room[self.myid]['item'][m['iid']] = [m['x'], m['y'], m['effect']]
                if m['type'] == 'track':
                    if m['obj'] == 'tank':
                        room[self.myid]['tank'] = [m['x'], m['y'], m['speed'], m['r']]
                if m['type'] == 'destroy':
                    print 'destroy'
                    room[self.myid]['tank'] = []
                    room[self.myid]['bullet'] = {}
                    room[self.myid]['score'] = 0
                    room[m['killer']]['score'] += 100
                if m['type'] == 'vanish':
                    print 'vanish'
                    if m['obj'] == 'tank':
                        room[self.myid]['tank'] = []
                    if m['obj'] == 'bullet':
                        room[self.myid]['bullet'][m['bid']] = []
                    if m['obj'] == 'item':
                        room[self.myid]['item'][m['iid']] = []
                    print room
                TankServer.send_updates(message)
            except Exception as e:
                print e

    def on_close(self):
        print "closed"
        self.connection_closed = True
        TankServer.waiters.remove(self)
        TankServer.send_updates(json.dumps({'type': 'defect', 'id': self.myid, 'name': room[self.myid]['name']}))
        #TankServer.send_updates(json.dumps({'type': 'vanish', 'obj': 'tank', 'id': self.myid}))
        room.pop(self.myid)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


def main():
    tornado.options.parse_command_line()
    app = Application()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()